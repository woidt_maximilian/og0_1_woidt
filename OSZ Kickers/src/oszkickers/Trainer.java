package oszkickers;

public class Trainer extends Personenkreise {

	private static String lizenzklasse;
	private static double aufwandsEntschaedigung;
	private static int betrag;

	public static void main(String[] args) {
	}

	Trainer() {
	}

	public static String getLizenzklasse() {
		return lizenzklasse;
	}

	public static void setLizenzklasse(String lizenzklasse) {
		Trainer.lizenzklasse = lizenzklasse;
	}

	public static double getAufwandsEntschaedigung() {
		return aufwandsEntschaedigung;
	}

	public static void setAufwandsEntschaedigung(double aufwandsEntschaedigung) {
		Trainer.aufwandsEntschaedigung = aufwandsEntschaedigung;
	}

	public static int getBetrag() {
		return betrag;
	}

	public static void setBetrag(int betrag) {
		Trainer.betrag = betrag;
	}

}
