package oszkickers;

public class Personenkreise extends Fußballverein {

	protected static String name;
	protected static int telefonnummer;

	

	Personenkreise() {
	}

	public static String getName() {
		return name;
	}

	public static void setName(String name) {
		Personenkreise.name = name;
	}

	public static int getTelefonnummer() {
		return telefonnummer;
	}

	public static void setTelefonnummer(int telefonnummer) {
		Personenkreise.telefonnummer = telefonnummer;
	}

}
