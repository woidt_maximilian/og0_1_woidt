import java.util.ArrayList; // import the ArrayList class

public class KeyStore01 {

	ArrayList<String> array = new ArrayList<String>();
	private String[] keylist;
	private int currentPos;
	public static final int MAXPOSITION = 100;
	public final int NICHTGEFUNDEN = -1;

	public KeyStore01() {
		this.keylist = new String[MAXPOSITION];
		this.currentPos = 0;

	}

	public KeyStore01(int length) {
		this.keylist = new String[length];
		this.currentPos = 0;
	}

	public boolean add(java.lang.String eintrag) {
		if (currentPos < MAXPOSITION) {
			this.keylist[this.currentPos] = eintrag;
			this.currentPos++;
			return true;
		} else {
			return false;
		}
	}

	// lineare Suche
	public int indexOf(java.lang.String eintrag) {
		for (int i = 0; i < this.currentPos; i++) {
			if (this.keylist[i].equals(eintrag))
				return i;
		}
		return NICHTGEFUNDEN;
	}

	public void remove(int index) {
		if (index >= 0 && index <= currentPos) {
			for (int i = index; i < currentPos; i++) {
				this.keylist[i] = this.keylist[i + 1];
				this.keylist[this.keylist.length - 1] = "";
			}
			currentPos--;
		}
	}

}
