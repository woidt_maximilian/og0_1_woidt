package de.oszimt.starsim2099;

/**
 * Write a description of class Ladung here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Ladung {

	// Attribute
	double posX;
	double posY;
	int masse;
	String typ;

	// Methoden
	public Ladung() {
		this.posX = posX;
		this.posY = posY;
		this.masse = masse;
		this.typ = typ;
	}

	public void setposX(double posX) {
		this.posX = posX;
	}

	public double getposX() {
		return posX;

	}

	public void setposY(double posX) {
		this.posY = posY;
	}

	public double getposY() {
		return posY;
	}

	public void setmasse(int masse) {
		this.masse = masse;
	}

	public int getmasse() {
		return masse;
	}

	public void settyp(String typ) {
		this.typ = typ;
	}

	public String gettyp(){
		return typ;
	}
	
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}
