
public class Addon {
	// Attribute
	private int idnummer;
	private String bezeichnung;
	private double verkaufspreis;
	private String art;
	private int anzahl;

	public Addon() {

	}

	public Addon(int idnummer, String bezeichnung, double verkaufspreis, String art, int anzahl) {
		idnummer = this.idnummer;
		bezeichnung = this.bezeichnung;
		verkaufspreis = this.verkaufspreis;
		art = this.art;
		anzahl = this.anzahl;
	}

	// Methoden
	public void setidnummer(int idnummer) {
		idnummer = this.idnummer;
	}

	public int getidnummer() {
		return idnummer;
	}

	public void setbezeichnung(String bezeichnung) {
		bezeichnung = this.bezeichnung;
	}

	public String getbezeichnung() {
		return bezeichnung;
	}

	public void setverkaufspreis(double verkaufspreis) {
		verkaufspreis = this.verkaufspreis;
	}

	public double getverkaufspreis() {
		return verkaufspreis;
	}

	public void setart(String art) {
		art = this.art;
	}

	public String getart() {
		return art;
	}

	public void setanzahl(int anzahl) {
		anzahl = this.anzahl;
	}

	public int getanzahl() {
		return anzahl;
	}

	public void aendereBestand(int aendereBestand) {
		anzahl = aendereBestand;
	}

	public void gesamtwert(double gesamtwert) {
		gesamtwert = anzahl * verkaufspreis;
	}
}
