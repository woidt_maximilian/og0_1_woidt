package omnom;

public class Haustier {
	// Attribute
	private int hunger;
	private int gesundheit;
	private int zufriedenheit;
	private int muede;

	// Methoden
	public Haustier() {

	}

	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {
		this.hunger = hunger;
	}

	public double getGesundheit() {
		return gesundheit;
	}

	public void setGesundheit(int gesundheit) {
		this.gesundheit = gesundheit;
	}

	public int getZufriedenheit() {
		return zufriedenheit;
	}

	public void setZufriedenheit(int zufriedenheit) {
		this.zufriedenheit = zufriedenheit;
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {
		this.muede = muede;
	}
}
