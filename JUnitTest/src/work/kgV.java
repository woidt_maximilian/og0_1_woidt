package work;

public class kgV {

	public static void main(String[] args) {

		int a = 6;

		int b = 7;

		int aspeicher = a;
		int bspeicher = b;

		while (aspeicher != bspeicher) {
			if (aspeicher < bspeicher) {
				aspeicher += a;
			} else {
				bspeicher += b;
			}
		}

		System.out.println("KGV: " + aspeicher);

	}

	public static int kgV(int a, int b) {

		int z1 = 6;

		int z2 = 7;

		int z1speicher = z1;
		int z2speicher = z2;

		while (z1speicher != z2speicher) {
			if (z1speicher < z2speicher) {
				z1speicher += z1;
			} else {
				z2speicher += z2;
			}
		}
		return z1speicher;
	}

}
