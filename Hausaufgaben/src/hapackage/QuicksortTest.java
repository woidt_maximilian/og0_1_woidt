package hapackage;

import java.util.Arrays;

public class QuicksortTest {
	public static void main(String[] args) {
		Quicksort qs = new Quicksort();
		int[] sortieren = { 7, 2, 4, 8, 6, 1, 3, 5, 9 };
		System.out.println(Arrays.toString(sortieren));
		qs.sort(sortieren, 0, sortieren.length - 1);
		System.out.println(Arrays.toString(sortieren));
		System.out.println(qs.getVertauschungen());

	}
}
