package hapackage;

import java.util.Timer;
import java.util.TimerTask;

/* 
    n = 762935429 true
	n = 862935439 true
	n = 962935439 true
	n = 1062935399 true
    n = 1162935517 true

*/
public class TestPrimzahl {

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		System.out.println(Primzahl.isPrimzahl(962935439L)); 
		long ende = System.currentTimeMillis();
		System.out.println(ende - start); //Zeit f�r den Ablauf wird berechnet in Millisekunden
	}											

	public static void uhr() {
		Timer timer = new Timer();
		TimerTask task = new TimerTask() {
			 public int i = 0;
			    public void run()
			    {
			        System.out.println("Timer ran " + ++i);
			    }
		};	
		timer.schedule(task, 0);		
	}	
}//end of class