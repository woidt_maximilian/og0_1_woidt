package hapackage;

public class Primzahl {
	public static boolean isPrimzahl(long zahl) {

		boolean ergebnis = false;
		for (long i = zahl - 1; i > 1; i--) {
			if (zahl % i == 0) {
				return false;
			}
		}
		return true;
	}
//end of class
}
