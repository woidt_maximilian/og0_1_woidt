package Test;

import java.util.Scanner;

public class StoppUhr {
    boolean running = false;
    Thread uhr = new Thread() {
        int sec = 0;
        public void run() {
            while (running) {
                System.out.print("\033[H\033[2J");
                System.out.flush();
                System.out.print(sec);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                ++sec;
            }
        }
    };

    private void steuerDieUhr() {
        Scanner sc = new Scanner(System.in);
        String s;
        while (true) {
            s = sc.next();
            if (s.equals("q")) {
                stop();
                sc.close();
                System.exit(0);
            }
            if (s.equals("s")) {
                start();
            }
        }
    }

    public void stop() {
        running = false;
    }

    public void start() {
        running = true;
        uhr.start();
    }

    public static void main(String[] args) {
        new StoppUhr().steuerDieUhr();
    }
}
