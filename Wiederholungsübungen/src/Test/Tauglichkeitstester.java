package Test;

public abstract class Tauglichkeitstester {

	public abstract void warten();

	public abstract void getErgebnis();

	public abstract void isAktiv();

	public abstract void starten();

	public abstract void stoppen();

	public abstract void zeigeHilfe();

}
